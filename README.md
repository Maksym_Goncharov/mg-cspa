
Prerequisites
-------------

- nodejs
- gulp and gulp-cli

Running
-------

Install dependencies:

> npm install

`node_modules` directory should be created during the install.

Install bower components:

> bower install

`bower_components` directory should be created during the install.

Build the project:

> gulp serve

