(function () {
    'use strict';

    angular
        .module('mgSpa')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('clinical-info', {
                url: '/',
                templateUrl: 'app/components/clinical-info/clinical-info.html',
                controller: 'ClinicalInfoController',
                controllerAs: 'ClinicalInfoCtrl'
            })
            .state('add-diagnose', {
                url: '/add-diagnose',
                templateUrl: 'app/components/add-diagnose/add-diagnose.html',
                controller: 'AddDiagnoseController',
                controllerAs: 'AddDiagnoseCtrl'
            })
            .state('edit-patient', {
                url: '/edit-patient',
                templateUrl: 'app/components/edit-patient/edit-patient.html',
                controller: 'EditPatientController',
                controllerAs: 'EditPatientCtrl'
            });

        $urlRouterProvider.otherwise('/');
    }

})();
