(function () {
    'uss strict';
    angular
        .module('addDiagnose')
        .controller('AddDiagnoseController', AddDiagnoseController);

    AddDiagnoseController.$inject = ['moment', 'dataService', '$state'];
    function AddDiagnoseController(moment, dataService, $state) {
        var vm = this;
        vm.addNewDiagnose = addNewDiagnose;
        vm.cancel = cancel;
        vm.dataService = dataService;

        function addNewDiagnose(data) {
            data.additionalDate = moment().format("DD/MM/YY");
            data.id = Math.floor(Math.random() * (99999999 - 11111111 + 1)) + 11111111;

            vm.dataService.updateDiagnose(data);
            $state.go('clinical-info', {}, {reload: true});
        }

        function cancel() {
            $state.go('clinical-info', {}, {reload: true});
        }
    }
})();