(function () {
    'use strict';
    angular
        .module('addDiagnose')
        .directive('addDiagnoseDirective', addDiagnoseDirective);

    function addDiagnoseDirective() {
        var config = {
            restrict: 'E',
            templateUrl: 'app/components/add-diagnose/add-diagnose.html',
            scope: {},
            controller: 'AddDiagnoseController',
            controllerAs: 'AddDiagnoseCtrl'
        };
        return config;
    }
})();