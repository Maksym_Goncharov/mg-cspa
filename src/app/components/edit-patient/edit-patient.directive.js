(function () {
    'use strict';
    angular
        .module('editPatient')
        .directive('editPatientDirective', editPatientDirective);

    function editPatientDirective() {
        var config = {
            restrict: 'E',
            templateUrl: 'app/components/edit-patient/edit-patient.html',
            scope: {},
            controller: 'EditPatientController',
            controllerAs: 'EditPatientCtrl'
        };
        return config;
    }
})();