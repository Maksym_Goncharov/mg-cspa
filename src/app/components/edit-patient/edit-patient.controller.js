(function () {
    'uss strict';
    angular
        .module('editPatient')
        .controller('EditPatientController', EditPatientController);

    EditPatientController.$inject = ['dataService', '$state'];
    function EditPatientController(dataService, $state) {
        var vm = this;
        vm.setPatientInfo = setPatientInfo;
        vm.dataService = dataService;

        function setPatientInfo(data) {
            var date = data.dob;
            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();
            data.birthday = day + '/' + month + '/' + year;
            vm.dataService.setPatientInfo(data);
            $state.go('clinical-info', {}, { reload: true });
        }
    }
})();