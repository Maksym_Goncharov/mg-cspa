(function () {
    'use strict';
    angular.module('patientInfo')
        .directive('patientInfoDirective', patientInfoDirective);

    function patientInfoDirective() {
        var config = {
            restrict: 'A',
            templateUrl: 'app/components/patient-info/patient-info.html',
            scope: {},
            controller: 'PatientInfoController',
            controllerAs: 'PatientInfoCtrl'
        };
        return config;
    }
})();