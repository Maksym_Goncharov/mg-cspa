(function () {
    'use strict';
    angular
        .module('patientInfo')
        .controller('PatientInfoController', PatientInfoController);

    PatientInfoController.$inject = ['dataService'];
    function PatientInfoController(dataService) {
        var vm = this;
        vm.dataService = dataService;
        vm.patientInfo = vm.dataService.getPatientInfo();
    }
})();