(function () {
    'use strict';
    angular
        .module('header')
        .directive('headerDirective', headerDirective);

    function headerDirective() {
        var config = {
            restrict: 'A',
            templateUrl: '/app/components/header/header.html',
            scope: {}
        };
        return config;
    }
})();