(function () {
    'use strict';
    angular
        .module('clinicalInfo')
        .directive('clinicalInfoDirective', clinicalInfoDirective);

    function clinicalInfoDirective() {
        var config = {
            restrict: 'E',
            templateUrl: 'app/components/clinical-info/clinical-info.html',
            scope: {},
            controller: 'ClinicalInfoController',
            controllerAs: 'ClinicalInfoCtrl'
        };
        return config;
    }
})();