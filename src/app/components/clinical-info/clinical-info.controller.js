(function () {
    'use strict';
    angular
        .module('clinicalInfo')
        .controller('ClinicalInfoController', ClinicalInfoController);

    ClinicalInfoController.$inject = ['dataService', 'moment', '$state'];

    function ClinicalInfoController(dataService, moment, $state) {
        var vm = this;
        vm.dataService = dataService;
        vm.currentDiagnoses = vm.dataService.getCurrentDiagnoses();
        vm.diagnosesHistory = vm.dataService.getDiagnosesHistory();
        vm.deleteDiagnose = deleteDiagnose;

        function deleteDiagnose(id){
            vm.currentDiagnoses.forEach(function (item, i, arr) {
                if (item.id === id) {
                    arr.splice(i, 1);
                    vm.dataService.setDiagnose(arr);
                    item.removalDate = moment().format("DD/MM/YY");
                    vm.dataService.updateDiagnosesHistory(item);
                    $state.reload();
                }
            });
        }
    }
})();
