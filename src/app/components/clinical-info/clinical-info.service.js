(function () {
    'use strict';
    angular
        .module('clinicalInfo')
        .service('clinicalInfoService', clinicalInfoService);
    function clinicalInfoService() {
        var service = {
            getClinicalInfo: getClinicalInfo
        };
        return service;

        function getClinicalInfo() {
            var info = {
                currentDiagnoses: [
                    {
                        id: 1,
                        code: 12345,
                        diagnosis: 'Asthma',
                        additionalDate: '12/12/16'
                    },
                    {
                        id: 2,
                        code: 23452,
                        diagnosis: 'Asthma werwe werwer',
                        additionalDate: '13/13/16'
                    },
                    {
                        id: 3,
                        code: 45789,
                        diagnosis: 'Asthma werwe werwer wer ewqr qew ',
                        additionalDate: '13/13/16'
                    }
                ],
                diagnosesHistory: [
                    {
                        id: 1,
                        code: 12345,
                        diagnosis: 'Asthma',
                        additionalDate: '12/12/16',
                        removalDate: '13/13/16'
                    },
                    {
                        id: 2,
                        code: 23452,
                        diagnosis: 'Asthma werwe werwer',
                        additionalDate: '13/13/16',
                        removalDate: '13/13/16'
                    },
                    {
                        id: 3,
                        code: 45789,
                        diagnosis: 'Asthma werwe werwer wer ewqr qew ',
                        additionalDate: '13/13/16',
                        removalDate: '13/13/16'
                    }
                ]
            };
            return info;
        }
    }
})();