(function () {
    'use strict';
    angular
        .module('mgSpa')
        .controller('BreadcrumbController', BreadcrumbController);

    BreadcrumbController.$inject = ['dataService'];

    function BreadcrumbController(dataService) {
        var vm = this;
        vm.dataService = dataService;
        vm.patientInfo = vm.dataService.getPatientInfo();
    }
})();