(function () {
    'use strict';
    angular
        .module('mgSpa')
        .directive('breadcrumbDirective', breadcrumbDirective);
    function breadcrumbDirective() {
        var config = {
            restrict: 'A',
            templateUrl: 'app/components/breadcrumb/breadcrumb.html',
            scope: {},
            controller: 'BreadcrumbController',
            controllerAs: 'BreadcrumbCtrl'
        };
        return config;
    }
})();