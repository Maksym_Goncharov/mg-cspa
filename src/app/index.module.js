(function () {
    'use strict';

    angular
        .module('mgSpa', [
            'ui.router',
            'ngMaterial',
            'toastr',
            'header',
            'patientInfo',
            'clinicalInfo',
            'addDiagnose',
            'editPatient'
        ]);

})();
