/* global moment:false */
(function() {
  'use strict';

  angular
    .module('mgSpa')
    .constant('moment', moment);

})();
