(function () {
    'use strict';
    angular
        .module('mgSpa')
        .service('dataService', dataService);
    function dataService() {
        var service = {
            getCurrentDiagnoses: getCurrentDiagnoses,
            getDiagnosesHistory: getDiagnosesHistory,
            getPatientInfo: getPatientInfo,
            updateDiagnose: updateDiagnose,
            updateDiagnosesHistory: updateDiagnosesHistory,
            setDiagnose: setDiagnose,
            setPatientInfo: setPatientInfo
        };
        return service;

        function getCurrentDiagnoses() {
            var info = angular.fromJson(localStorage.getItem("currentDiagnoses"));
            return info;
        }

        function getDiagnosesHistory() {
            var info = angular.fromJson(localStorage.getItem("diagnosesHistory"));
            return info;
        }

        function getPatientInfo() {
            var info = angular.fromJson(localStorage.getItem("patientInfo"));
            return info;
        }

        function updateDiagnose(data) {
            var localData = angular.fromJson(localStorage.getItem("currentDiagnoses"));
            var serialObj;
            var arr = [];
            if (localData) {
                arr.push(data);
                localData.forEach(function(item) {
                    arr.push(item);
                });
                serialObj = angular.toJson(arr);
            } else {
                arr.push(data);
                serialObj = angular.toJson(arr);
            }
            localStorage.setItem("currentDiagnoses", serialObj);
        }

        function updateDiagnosesHistory(data) {
            var localData = angular.fromJson(localStorage.getItem("diagnosesHistory"));
            var serialObj;
            var arr = [];
            if (localData) {
                arr.push(data);
                localData.forEach(function(item) {
                    arr.push(item);
                });
                serialObj = angular.toJson(arr);
            } else {
                arr.push(data);
                serialObj = angular.toJson(arr);
            }
            localStorage.setItem("diagnosesHistory", serialObj);
        }

        function setDiagnose(arr) {
            var serialObj = angular.toJson(arr);
            localStorage.setItem("currentDiagnoses", serialObj);
        }

        function setPatientInfo(data) {
            var serialObj = angular.toJson(data);
            localStorage.setItem("patientInfo", serialObj);
        }
    }
})();